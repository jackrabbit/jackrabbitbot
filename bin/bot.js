'use strict';

var Bot = require('slackbots');
var path = require('path');
var pg = require('pg');

var token = process.env.BOT_API_KEY;
var dbPath = process.env.DATABASE_URL;
var db = null;

var botName = "Jack"; 
var settings = {
    token: token,
    name: botName
};

var params = {
        icon_emoji: ':roborabbit:'
};


var sendFirstMessage = true;
var bot = new Bot(settings);

bot.on('start', function() {
    connectToDataBase();
});

bot.on('message', function(data) {
    if (typeof data.text == "string" && 
        data.username != botName){
        if(typeof getChannelById(data.channel) != 'undefined' && 
        getChannelById(data.channel).name == 'bot_admins'){
            handleMessageFromAdmins(data);
        }
        else if(data.text.indexOf("[ADD-QUESTION]")>-1){
            replyAddedQuestion(data);
        }
        else if(data.text.indexOf("[ADD-ANSWER]")>-1){
            replyAddedAnswer(data);
        }
        else {
            checkIfDirectOrChannel(data);
        }
    } 
});

function handleMessageFromAdmins(data){
    if(data.text.indexOf("[ALL-ANSWERS]")>-1){
        replyAllQuestions(data);
    }
    else if(data.text.indexOf("[ALL-QUESTIONS]")>-1){
        replyAllAnswersPerQuestion(data);
    }
    else if(data.text.indexOf("[PENDING-APPROVAL]")>-1){
        replyQuestionsPendingApproval(data);
    }
    else if(data.text.indexOf("[APPROVE]")>-1){
        replyApprovedAnswer(data);
    }
    else if(data.text.indexOf("[DELETE-ANSWER]")>-1){
        replyDeletedAnswer(data);
    }
    else if(data.text.indexOf("[DELETE-QUESTION]")>-1){
        replyDeletedQuestion(data);
    }    
    else if(data.text.indexOf("[ADD-QUESTION]")>-1){
        replyAddedQuestion(data);
    }
    else if(data.text.indexOf("[ADD-ANSWER]")>-1){
        replyAddedAnswer(data);
    }
    else if(data.text.indexOf("[EDIT-QUESTION]")>-1){
        replyEdditedQuestion(data);
    }
    else if(data.text.indexOf("[EDIT-ANSWER]")>-1){
        replyEdditedAnswer(data);
    }
}

function replyEdditedAnswer(data){
    var rawText = data.text.replace('“', '"').replace('”','"');
    var answerText = encodeURI(rawText.match(/\"(.*?)\"/)[1]);
    var remainingText = rawText.substring(0, rawText.indexOf('"'));
    var answerKey = parseInt(remainingText.replace(/[^0-9\.]/g, ''));
    db.query("UPDATE answers SET answer = $1 WHERE key = $2",[answerText, answerKey], function(err, answer){
        if(err){
            return console.error('DATABASE ERROR:', err);
        }
        var channel = getChannelById(data.channel);
        bot.postMessageToChannel(channel.name, "Editted answer with id "+answerKey, params);
    });
}

function replyEdditedQuestion(data){
    var rawText = data.text.replace('“', '"').replace('”','"');
    var questionText = encodeURI(rawText.match(/\"(.*?)\"/)[1]);
    var remainingText = rawText.substring(0, rawText.indexOf('"'));
    var questionKey = parseInt(remainingText.replace(/[^0-9\.]/g, ''));
    db.query("UPDATE questions SET question = $1 WHERE key = $2",[questionText, questionKey], function(err, answer){
        if(err){
            return console.error('DATABASE ERROR:', err);
        }
        var channel = getChannelById(data.channel);
        bot.postMessageToChannel(channel.name, "Editted question with id "+questionKey, params);
    });
}

function replyAddedAnswer(data){
    var rawText = data.text.replace('“', '"').replace('”','"');
    var answerText = encodeURI(rawText.match(/\"(.*?)\"/)[1]);
    var remainingText = rawText.substring(0, rawText.indexOf('"'));
    db.query("INSERT INTO answers (answer, is_approved) VALUES ($1, 0)",[answerText], function(err, answer){
        if(err){
            return console.error('DATABASE ERROR:', err);
        }
        db.query("SELECT * FROM answers ORDER BY key DESC", function(err, results){
            var channel = getChannelById(data.channel);
            var newAnswerText = "Answer added! Now to add some questions (text that will trigger this answer) send me some options following this syntaxt:\n[ADD-QUESTION]"+results.rows[0].key+". \"Text to trigger answer\"\nExample:\n[ADD-QUESTION]"+results.rows[0].key+". \"Where is the bathroom\"";
            if (typeof channel == "undefined"){
                var user = getUserById(data.user);
                bot.postMessageToUser(user.name, newAnswerText, params);
            }
            else{
                bot.postMessageToChannel(channel.name, newAnswerText, params);
            }
        });
    });
}

function replyAddedQuestion(data){
    var rawText = data.text.replace('“', '"').replace('”','"');
    var questionText = encodeURI(rawText.match(/\"(.*?)\"/)[1]);
    var remainingText = rawText.substring(0, rawText.indexOf('"'));
    var answerKey = parseInt(remainingText.replace(/[^0-9\.]/g, ''));
    db.query("INSERT INTO questions (question, answer_key) VALUES ($1, $2)",[questionText, answerKey], function(err, answer){
        if(err){
            return console.error('DATABASE ERROR:', err);
        }
        var channel = getChannelById(data.channel);
        if (typeof channel == "undefined"){
            var user = getUserById(data.user);
            bot.postMessageToUser(user.name, "Added question to answer with id "+answerKey, params);
        }
        else{
            bot.postMessageToChannel(channel.name, "Added question to answer with id "+answerKey, params);
        }
    });
}

function replyDeletedAnswer(data){
    var answerKey = parseInt(data.text.replace(/[^0-9\.]/g, ''));
    db.query("DELETE from answers WHERE key = "+answerKey, function(err, answer){
        if(err){
            return console.error('DATABASE ERROR:', err);
        }
        db.query("DELETE from questions WHERE answer_key = "+answerKey, function(err, answer){
            if(err){
                return console.error('DATABASE ERROR:', err);
            }
            var channel = getChannelById(data.channel);
            bot.postMessageToChannel(channel.name, "Answer with key "+answerKey+" deleted.", params);
        });
    });
}

function replyDeletedQuestion(data){
    var questionKey = parseInt(data.text.replace(/[^0-9\.]/g, ''));
    db.query("DELETE from questions WHERE key = "+questionKey, function(err, answer){
        if(err){
            return console.error('DATABASE ERROR:', err);
        }
        var channel = getChannelById(data.channel);
        bot.postMessageToChannel(channel.name, "Question with key "+questionKey+" deleted.", params);
    });
}

function replyApprovedAnswer(data){
    var answerKey = parseInt(data.text.replace(/[^0-9\.]/g, ''));
    db.query("UPDATE answers SET is_approved = 1 WHERE key = "+answerKey, function(err, result){
        if(err){
            return console.error('DATABASE ERROR:', err);
        }
        var channel = getChannelById(data.channel);
        bot.postMessageToChannel(channel.name, "Answer with key "+answerKey+" approved.", params);
    });
}

function replyQuestionsPendingApproval(data){
    var replyText = "";
    const query = db.query('SELECT * FROM answers WHERE is_approved = 0;');

    query.on('row', (row) => {
        replyText += "answer_id: "+row.key + ". Answer text: "+decodeURI(row.answer)+". Approved: No\n";
    });
    query.on('end', () => {
        var channel = getChannelById(data.channel);
        bot.postMessageToChannel(channel.name, replyText, params);
    });
}

function replyAllQuestions(data){
    var replyText = "";
    const query = db.query('SELECT * FROM answers;');
    query.on('row', (answer) => {
        replyText += "answer_id: "+answer.key + ". Answer text: "+decodeURI(answer.answer)+". Approved: ";
        if(answer.is_approved == 0){
            replyText +="No.";
        }
        else{
            replyText +="Yes."
        }
        replyText += "\n"
    });

    query.on('end', () => {
        var channel = getChannelById(data.channel);
        bot.postMessageToChannel(channel.name, replyText, params);
    });


    // var replyText = "";
    // db.query('SELECT * FROM answers', function (err, answers){
    //     if(err){
    //         return console.error('DATABASE ERROR:', err);
    //     }
    //     answers.forEach(function (answer){
    //         replyText += "answer_id: "+answer.key + ". Answer text: "+decodeURI(answer.answer)+". Approved: ";
    //         if(answer.is_approved == 0){
    //             replyText +="No.";
    //         }
    //         else{
    //             replyText +="Yes."
    //         }
    //         replyText += "\n"
    //     });
    //     var channel = getChannelById(data.channel);
    //     bot.postMessageToChannel(channel.name, replyText, params);

    // });
}

//postgres optimized
function replyAllAnswersPerQuestion (data){
    
    var replyText = "";
    var answerKey = parseInt(data.text.replace(/[^0-9\.]/g, ''));
    const query = db.query('SELECT * FROM questions WHERE answer_key='+answerKey);
    
    query.on('row', (question) => {
        replyText+= "question_id: "+question.key+". Question text: " + decodeURI(question.question) + "\n" 
    });

    query.on('end', () => {
        var channel = getChannelById(data.channel);
        bot.postMessageToChannel(channel.name, replyText, params);
    });


    // db.query('SELECT * FROM questions WHERE answer_key='+answerKey, function (err, questions) {
    //     if(err){
    //         return console.error('DATABASE ERROR:', err);
    //     }
    //     questions.forEach(function(question){
    //         replyText+= "question_id: "+question.key+". Question text: " + decodeURI(question.question) + "\n" 
    //     });
    //     var channel = getChannelById(data.channel);
    //     bot.postMessageToChannel(channel.name, replyText, params);
    // });

}

//postgres optimized
function checkIfDirectOrChannel(data){
    var channel = getChannelById(data.channel);
    var dataText = data.text.replace("@","")
    if (typeof channel == "undefined"){
        lookForQuestionInDatabase(data);
    }
    else if(dataText.indexOf("<U3XTND38B>")>-1){
        lookForQuestionInDatabase(data);
    }
}

function lookForQuestionInDatabase(data){
    const query = db.query('SELECT * FROM questions ;')
    var isFound = false;
    query.on('row', (row) => {
        var dataText = data.text.replace("@","");
        dataText = dataText.replace("<U3XTND38B>","");
        if(dataText.replace(/[^A-Z0-9]/ig, "").indexOf(decodeURI(row.question).toLowerCase().replace(/[^A-Z0-9]/ig, ""))>-1){
            replyWithAnswerId(row.answer_key, data);
            isFound = true;
            return;
        }
    });
    query.on('end', () => {
        if(!isFound && typeof getChannelById(data.channel) == "undefined"){
            replyMessage(data, "Ummm... What!? I haven't been programed to understand that.\nDo you want to teach me about it? If so, send me a message with what you want me to reply following this syntax:\n[ADD-ANSWER]\"answer text\"\nExample:\n[ADD-ANSWER]\"The bathroom is by the kitchen\"");
        }
    });

}

function replyWithAnswerId(answerId, data){
    db.query('SELECT * FROM answers WHERE key='+answerId, function(err, result){
        if (err){
            return console.error('DATABASE ERROR:', err);
        }
        if(result.rows[0].is_approved == 1){
            replyMessage(data, decodeURI(result.rows[0].answer));
        }
    });
}

function getUserById (userId) {
    return bot.users.filter(function (item) {
        return item.id === userId; 
    })[0];
};

function getChannelById (channelId) {
    return bot.channels.filter(function (item) {
        return item.id === channelId; 
    })[0];
};

function replyMessage (data, text){
    var channel = getChannelById(data.channel);
    var user = getUserById(data.user);
    if (typeof channel != "undefined"){
        bot.postMessageToChannel(channel.name, text+" @"+user.name, params);
    }
    
    else{
        bot.postMessageToUser(user.name, text, params);
    }

}

function connectToDataBase () {
    pg.connect(dbPath, function(err, client, done) {
        if (err){
            return console.error('DATABASE ERROR:', err);
        }
        db = client;
    });
}

function postDebugMessageToGibran(message){
    bot.postMessageToUser("gibran",message);
}
